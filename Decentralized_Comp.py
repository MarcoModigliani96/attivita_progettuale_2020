import nest_asyncio
nest_asyncio.apply()

import numpy as np
import tensorflow as tf
import tensorflow_federated as tff

from tensorflow_model_optimization.python.core.internal import tensor_encoding as te

MAX_CLIENT_DATASET_SIZE = 418
NUM_ROUND = 400
NUM_CLIENT = 200
CLIENT_EPOCHS_PER_ROUND = 5
CLIENT_BATCH_SIZE = 20
TEST_BATCH_SIZE = 500
NUM_ELEMENTS = 1000
RANDOM = True

# Take emnist data from TFF
emnist_train, emnist_test = tff.simulation.datasets.emnist.load_data(
    only_digits=True)

# Preprocess the data
def reshape_emnist_element(element):
  return (tf.expand_dims(element['pixels'], axis=-1), element['label'])

def preprocess_train_dataset(dataset):
  
  return (dataset
          # Shuffle according to the largest client dataset
          .shuffle(buffer_size=MAX_CLIENT_DATASET_SIZE)
          # Repeat to do multiple local epochs
          .repeat(CLIENT_EPOCHS_PER_ROUND)
          # Batch to a fixed client batch size
          .batch(CLIENT_BATCH_SIZE, drop_remainder=False)
          # Preprocessing step
          .map(reshape_emnist_element))

emnist_train = emnist_train.preprocess(preprocess_train_dataset)


# Creates the model
def create_original_fedavg_cnn_model(only_digits=True):
  # Uncomment to use CNN network
  
  # data_format = 'channels_last'

  # max_pool = functools.partial(
  #     tf.keras.layers.MaxPooling2D,
  #     pool_size=(2, 2),
  #     padding='same',
  #     data_format=data_format)
  # conv2d = functools.partial(
  #     tf.keras.layers.Conv2D,
  #     kernel_size=5,
  #     padding='same',
  #     data_format=data_format,
  #     activation=tf.nn.relu)

  # model = tf.keras.models.Sequential([
  #     tf.keras.layers.InputLayer(input_shape=(28, 28, 1)),
  #     conv2d(filters=32),
  #     max_pool(),
  #     conv2d(filters=64),
  #     max_pool(),
  #     tf.keras.layers.Flatten(),
  #     tf.keras.layers.Dense(512, activation=tf.nn.relu),
  #     tf.keras.layers.Dense(10 if only_digits else 62),
  #     tf.keras.layers.Softmax(),
  # ])

  # return model
  return tf.keras.models.Sequential([
      tf.keras.layers.Flatten(input_shape=(28, 28, 1)),
      tf.keras.layers.Dense(200, activation='relu'),
      tf.keras.layers.Dense(200, activation='relu'),
      tf.keras.layers.Softmax(),])

# Takes the specification of the data from a client. It's necessary to create a tff.learning 
input_spec = emnist_train.create_tf_dataset_for_client(
    emnist_train.client_ids[0]).element_spec

# Creates the tff model
def tff_model_fn():
  keras_model = create_original_fedavg_cnn_model()
  return tff.learning.from_keras_model(
      keras_model=keras_model,
      input_spec=input_spec,
      loss=tf.keras.losses.SparseCategoricalCrossentropy(),
      metrics=[tf.keras.metrics.SparseCategoricalAccuracy()])

#  Function for building encoded broadcast
def broadcast_encoder_fn(value):
 
  spec = tf.TensorSpec(value.shape, value.dtype)
  if value.shape.num_elements() > NUM_ELEMENTS:
    return te.encoders.as_simple_encoder(
        te.encoders.uniform_quantization(bits=8), spec)
  else:
    return te.encoders.as_simple_encoder(te.encoders.identity(), spec)

# Function for building encoded mean
def mean_encoder_fn(value):
  
  spec = tf.TensorSpec(value.shape, value.dtype)
  if value.shape.num_elements() > NUM_ELEMENTS:
    return te.encoders.as_gather_encoder(
        te.encoders.uniform_quantization(bits=8), spec)
  else:
    return te.encoders.as_gather_encoder(te.encoders.identity(), spec)



encoded_broadcast_process = (
    tff.learning.framework.build_encoded_broadcast_process_from_model(
        tff_model_fn, broadcast_encoder_fn))
encoded_mean_process = (
    tff.learning.framework.build_encoded_mean_process_from_model(
    tff_model_fn, mean_encoder_fn))

# Creates the federated calculation with the econders
federated_averaging_with_compression = tff.learning.build_federated_averaging_process(
    tff_model_fn,
    client_optimizer_fn=lambda: tf.keras.optimizers.SGD(learning_rate=0.02),
    server_optimizer_fn=lambda: tf.keras.optimizers.SGD(learning_rate=1.0),
    broadcast_process=encoded_broadcast_process,
    aggregation_process=encoded_mean_process)



# A helper function for creating a human-readable size
def format_size(size):
  
  size = float(size)
  for unit in ['bit','Kibit','Mibit','Gibit']:
    if size < 1024.0:
      return "{size:3.2f}{unit}".format(size=size, unit=unit)
    size /= 1024.0
  return "{size:.2f}{unit}".format(size=size, unit='TiB')


# Creates an environment that contains sizing information
def set_sizing_environment():
  
  # Creates a sizing executor factory to output communication cost
  # after the training finishes. Note that sizing executor only provides an
  # estimate (not exact) of communication cost, and doesn't capture cases like
  # compression of over-the-wire representations. However, it's perfect for
  # demonstrating the effect of compression in this tutorial.
  sizing_factory = tff.framework.sizing_executor_factory()

  # TFF has a modular runtime you can configure yourself for various
  # environments and purposes, and this example just shows how to configure one
  # part of it to report the size of things.
  context = tff.framework.ExecutionContext(executor_fn=sizing_factory)
  tff.framework.set_default_context(context)

  return sizing_factory




# Trains the federated averaging process and output metrics
def train(federated_averaging_process, num_rounds, num_clients_per_round, summary_writer):
 
  # Create a environment to get communication cost.
  environment = set_sizing_environment()

  # Initialize the Federated Averaging algorithm to get the initial server state.
  state = federated_averaging_process.initialize()

  with summary_writer.as_default():
    for round_num in range(num_rounds):
      # Sample the clients parcitipated in this round.
      if RANDOM == False:
          sampled_clients = emnist_train.client_ids[0:num_clients_per_round]
      else:
        sampled_clients = np.random.choice(
          emnist_train.client_ids,
          size=num_clients_per_round,
          replace=False)
      # Create a list of `tf.Dataset` instances from the data of sampled clients.
      sampled_train_data = [
          emnist_train.create_tf_dataset_for_client(client)
          for client in sampled_clients
      ]
      # Round one round of the algorithm based on the server state and client data
      # and output the new state and metrics.
      state, metrics = federated_averaging_process.next(state, sampled_train_data)


      size_info = environment.get_size_info()
      broadcasted_bits = size_info.broadcast_bits[-1]
      aggregated_bits = size_info.aggregate_bits[-1]

      print('round {:2d}, metrics={}, broadcasted_bits={}, aggregated_bits={}'.format(round_num, metrics, format_size(broadcasted_bits), format_size(aggregated_bits)))

      # Add metrics to Tensorboard.
      for name, value in metrics['train'].items():
          tf.summary.scalar(name, value, step=round_num)

      # Add broadcasted and aggregated data size to Tensorboard.
      tf.summary.scalar('cumulative_broadcasted_bits', broadcasted_bits, step=round_num)
      tf.summary.scalar('cumulative_aggregated_bits', aggregated_bits, step=round_num)
      summary_writer.flush()






logdir_for_compression = "\logdir_for_compression"
summary_writer_for_compression = tf.summary.create_file_writer(
    logdir_for_compression)

train(federated_averaging_process=federated_averaging_with_compression, 
      num_rounds=NUM_ROUND,
      num_clients_per_round=NUM_CLIENT,
      summary_writer=summary_writer_for_compression)