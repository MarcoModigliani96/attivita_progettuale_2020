import nest_asyncio
nest_asyncio.apply()

import numpy as np
import tensorflow as tf
import tensorflow_federated as tff


# Takes emnist data from TFF
emnist_train, emnist_test = tff.simulation.datasets.emnist.load_data(
    only_digits=True, cache_dir="\cache_dir")


# Makes one big dataset from all clients
emnist_train_ds = emnist_train.create_tf_dataset_from_all_clients()


print(emnist_train_ds.element_spec)


emnist_train_images_labels = emnist_train_ds.flat_map(lambda x: tf.data.Dataset.from_tensors((x['pixels'], x['label'])))

emnist_train_images_labels = emnist_train_images_labels.batch(32, drop_remainder=True )



# Uncomment to use TensorBoard

# log_dir = "\logdir\"
# tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
# file_writer = tf.summary.create_file_writer(log_dir + "/metrics")
# file_writer.set_as_default()



my_callbacks = [
    tf.keras.callbacks.EarlyStopping(monitor='accuracy', patience=5, restore_best_weights=True),
    #tf.keras.callbacks.TensorBoard(log_dir=log_dir),
]


model = tf.keras.Sequential([  
    tf.keras.layers.Flatten(input_shape=(28,28,1)),
    tf.keras.layers.Dense(200, activation='relu'),
    tf.keras.layers.Dense(200, activation='relu'),
    tf.keras.layers.Softmax(),
 ])

model.summary()

opt = tf.keras.optimizers.SGD(lr= 0.02)

model.compile(optimizer=opt,
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])


training_history = model.fit(emnist_train_images_labels.skip(1000),
                             validation_data = emnist_train_images_labels.take(1000) ,
                              epochs=35,   
                              batch_size = 32,                           
                              callbacks=my_callbacks)

print("Average test loss: ", np.average(training_history.history['loss']))

